package Fiis.uni.poo.SunandMoon.repository;

import Fiis.uni.poo.SunandMoon.entities.Users;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserRepository extends CrudRepository<Users,String>{
    
}

