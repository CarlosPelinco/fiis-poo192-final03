package Fiis.uni.poo.SunandMoon.controladores;
import Fiis.uni.poo.SunandMoon.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SendMailController {

    @Autowired
    private MailService mailService;


    @GetMapping("/contacto")
    public String contacto(){
       return "/contacto";
    }

    @PostMapping("/sendMail")
    public String sendMail(@RequestParam("name") String name, @RequestParam("mail") String mail, @RequestParam("subject") String subject, @RequestParam("body") String body){

        String message = body +"\n\nDatos de contacto: " + "\nNombre: "+name+"\nE-mail: " +mail;
        mailService.sendMail("jkevinfglml@gmail.com","jkfloresg@uni.pe",subject,message);

        return "/contacto";
    }
}
