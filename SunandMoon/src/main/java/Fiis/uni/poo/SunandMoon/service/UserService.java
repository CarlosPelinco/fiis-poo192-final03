
package Fiis.uni.poo.SunandMoon.service;

import Fiis.uni.poo.SunandMoon.entities.Users;


public interface UserService {
    public Iterable<Users> listAllUsers();
	
    public Users getUserByName(String username);

    public Users saveUser(Users users);

    public void deleteUser(String username);
    
}
