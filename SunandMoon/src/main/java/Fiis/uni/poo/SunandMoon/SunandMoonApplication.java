package Fiis.uni.poo.SunandMoon;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunandMoonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SunandMoonApplication.class, args);
	}

}
