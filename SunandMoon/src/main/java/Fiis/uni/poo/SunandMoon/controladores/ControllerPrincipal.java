package Fiis.uni.poo.SunandMoon.controladores;

import Fiis.uni.poo.SunandMoon.entities.Users;
import Fiis.uni.poo.SunandMoon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Controller
public class ControllerPrincipal {


    @RequestMapping("/")
    public String index() {
    return "index";
    }


    @RequestMapping("/Registro")
    public String Registro(Model model){
        model.addAttribute("users", new Users(""));
       model.addAttribute("message",false);
        return "Registro";
    }
    @Autowired
    private UserService userService;


    @RequestMapping(value = "user", method = RequestMethod.POST)
    public String saveUser(Users users, Model model) {
        userService.saveUser(users);
        model.addAttribute("message", true);
        return "/Registro";
    }


   
    @RequestMapping("/login")
    public String login(){
        return "login";
    }


    @RequestMapping("/Hoteles")
    public String Hoteles(){
        return "Hoteles";
    }

    @RequestMapping("/Tours")
    public String Tours(){
        return "Tours";
    }
    @Autowired
    private JdbcTemplate template;

    @PostMapping("/RegistroPrueba")
    public String RegistroPRUEBA

            (@RequestParam String username , String password, String firstname, String lastname, String email, String phone) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "INSERT INTO usersprueba(username,password,firstname,lastname,email,phone) VALUES (?,?,?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (username));
        pst.setString(2, (password));
        pst.setString(3, (firstname));
        pst.setString(4, (lastname));
        pst.setString(5, (email));
        pst.setString(6, (phone));
        pst.executeUpdate();
        return "Registro exitoso";
    }

    @PostMapping("/loginPRUEBA")
    public String loginPRUEBA
            (@RequestParam String username, String password) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "SELECT * FROM usersprueba (username,password)=(?,?) ";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (username));
        pst.setString(2, (password));
        ResultSet rs = pst.executeQuery();
        if (rs.next() == false){
            return "Contraseña y/o Nombredeusuario incorrectos";
        } else {
            return "Has inciado sesión";
        }
    }

    @PostMapping ("/ReservarHotel")
    public  String reservarHotel(@RequestParam String namehotel, Integer type ,Integer numberofrooms,Integer numberofguests,Date startdate,
                                 Date enddate   ) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String query = "INSERT INTO reservahotel VALUES(DEFAULT,?, ?, ?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (namehotel));
        pst.setInt(2, (type));
        pst.setInt(3, (numberofrooms));
        pst.setInt(4, (numberofguests));
        pst.setDate(5, (startdate));
        pst.setDate(6, (enddate));
        //pst.executeUpdate();
        pst.execute();
        return "Hoteles";
    }
    @RequestMapping("/Hotels/package")
    public String costoreservahotel(){

        String sql ="SELECT tipo,canthabitaciones, " +
                "tipo*canthabitaciones FROM reservahotel";

        String sql2="SELECT fechainicio,fechafin, " +
                "EXTRACT(DAY FROM age(date(fechafin),date(fechainicio) ) ) as dif_dias FROM reservahotel";

        int duracion = template.queryForObject(sql2, Integer.class);
        int preciopordia= template.queryForObject(sql, Integer.class);

        int preciototal=duracion*preciopordia;
        return "<h1> El viaje cuesta" +  preciototal + " :D aún puede seguir agregando</h1>";
    }




}
