package Fiis.uni.poo.SunandMoon.entities;


import java.sql.Date;

public class ReservaHotelRequest {

    private String nombrehotel;
    private Integer tipo;
    private Integer canthabitaciones;
    private Integer canthuespedes;
    private Date fechainicio;
    private Date fechafin;

    public String getNombrehotel() {
        return nombrehotel;
    }

    public void setNombrehotel(String nombrehotel) {
        this.nombrehotel = nombrehotel;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getCanthabitaciones() {
        return canthabitaciones;
    }

    public void setCanthabitaciones(Integer canthabitaciones) {
        this.canthabitaciones = canthabitaciones;
    }

    public Integer getCanthuespedes() {
        return canthuespedes;
    }

    public void setCanthuespedes(Integer canthuespedes) {
        this.canthuespedes = canthuespedes;
    }

    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    public Date getFechafin() {
        return fechafin;
    }

    public void setFechafin(Date fechafin) {
        this.fechafin = fechafin;
    }
}
