package Fiis.uni.poo.SunandMoon.seguridad;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableAutoConfiguration
public class SecurityConfig extends WebSecurityConfigurerAdapter {



    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests().antMatchers("/","/index","/css/**","/js/**","/img/**","/Registro","/login","/user","/sendMail","/contacto","/Hoteles","/Tours","/Hoteles/**","/Tours/**","/paquetes","/ReservarHotel").permitAll()
                .and()
                .formLogin().loginPage("/login").defaultSuccessUrl("/")
                .and()
                .logout()
                .permitAll();
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select username,password, enabled from users where username=?")
                .authoritiesByUsernameQuery("select username, role from user_roles where username=?");
    }
}
    /**
     El metodo authorizeRequests() permite restringir y/o dar acceso request HTTP
     antMatchers(): Lista de URL que corresponden a un RequestMapping como lo hacemos en los controladores.
     permitAll(): Especifica que estas URLs son accesibles por cualquiera.
     access(): permite el acceso cumpliendo la expresión, en este caso tenemos la expresion “hasRole()”. Donde verifica si el usuario tiene ese especifico Role.
     anyRequest(): Ya que la configuración es lineal poniendo este metodo al final interpreta los request a las URLs que no fueron descritos, y en conjunto con el metodo authenticated() permite y da acceso a cualquier usuario que este autenticado.
     El metodo fromLogin(). Permite personalizar el proceso de inicio de sesión
     loginPage(): indica la url de la pagina de inicio de sesión
     defaultSuccessUrl(): Indica a cual URL sera redirigido cuando el usuario inicie sesión.
     failureUrl(): Indica a cual URL sera redirigido cuando el inicio de sesión falla.
     usernameParameter() y passwordParameter(): Indica el nombre de los parámetros respectivamente.
     El metodo logout(): Personaliza el proceso de cierre de sesión.
     logoutSuccessUrl(): Indica la URL donde sera redirigido cuando el usuario cierre sesión.
     */


