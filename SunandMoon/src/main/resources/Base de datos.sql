
CREATE TABLE usersprueba(
    username varchar(20),
    password varchar(20) not null,
    firstname text not null,
    lastname text not null,
    email varchar(10) not null,
    phone varchar(10) not null,
    CONSTRAINT pk_usersprueba PRIMARY KEY (username));


CREATE TABLE  reservahotelprueba
(   codigo smallserial,
    codigohotel smallint not null,
    tipo integer not null,
    canthabitaciones integer not null,
    canthuespedes integer not null,
    fechainicio date not null,
    fechafin date not null,
    CONSTRAINT pk_reservahotel PRIMARY KEY (codigo),
    CONSTRAINT fk_reservahotel_HOTEL FOREIGN KEY(codigohotel) REFERENCES HOTEL(codigo)
)
;


CREATE TABLE HOTEL(
    codigo smallserial,
    nombrehotel varchar(30) not null,
    num_habitaciones int,
    direccion int,
    telefono varchar(15),
    valoracion double precision,
    CONSTRAINT pk_HOTEL PRIMARY KEY (codigo));

CREATE TABLE  reservahotel
(   codigo smallserial,
    nombrehotel varchar(20) not null,
    tipo integer not null,
    canthabitaciones integer not null,
    canthuespedes integer not null,
    fechainicio date not null,
    fechafin date not null,
    CONSTRAINT pk_reservahotel PRIMARY KEY (codigo)
);


CREATE TABLE IF NOT EXISTS reservausuario
(  username varchar(20),
   codigo smallint not null,
   CONSTRAINT pk_reservausuario PRIMARY KEY(username,codigo),
   constraint fk_reservausuario_users FOREIGN KEY(username) REFERENCES  users (username),
   constraint fk_reservausuario_reservahotel FOREIGN KEY(codigo) REFERENCES reservahotel(codigo)
       ON UPDATE RESTRICT ON DELETE RESTRICT
);










INSERT INTO reservausuario(username,codigo)
VALUES
('Jair',9);

INSERT INTO reservausuario(username,codigo)
VALUES
('Jair',10);


