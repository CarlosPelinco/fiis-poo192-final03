function valida(){
    var nombre,correo,tema,mensaje,expresion;
    nombre=document.getElementById("name").value;
    correo=document.getElementById("mail").value;
    tema=document.getElementById("subject").value;
    mensaje=document.getElementById("body").value;

    expresion = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if(nombre === "" || correo ==="" || tema === "" || mensaje === "" ) {

        alert("Todos los campos son obligatorios")
        return false;
    }
    else if(nombre.length>30){
        alert("El nombre es muy largo");
        return false;
    }
    else if(correo.length>30){
        alert("El correo es muy largo");
        return false;
    }
    else if(!expresion.test(correo)){
        alert("El correo electronico no es válido");
        return false;
    }
    else if (tema.length>20){
        alert("El tema es muy largo");
        return false;
    }
    else if(mensaje.length>600){
        alert("El mensaje es muy largo");
        return false;
    }

    else{
        alert("Tu mensaje ha sido enviado. Por favor espere la respuesta de nuestro equipo Sun and Moon")
    }


}


