   //mapa regular
   function regular_map() {
    var var_location = new google.maps.LatLng(-12.015479,-77.050391);
    var var_mapoptions = {
        center: var_location,
        zoom: 18
    };
    var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);
    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "Son and Moon"
    });
}

// Inicia el mapa
google.maps.event.addDomListener(window, 'load', regular_map);